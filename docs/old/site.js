"use strict";

var Cookie = function() {

};

Cookie.prototype.getCookie = function(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));

    return matches ? decodeURIComponent(matches[1]) : '';
};

Cookie.prototype.setCookie = function(name, value, options) {
    options = options || {expires: 3600 * 24, path: '/'};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires*1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for(var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
};

Cookie.prototype.deleteCookie = function (name) {
    this.setCookie(name, "", { expires: -1 })
};

var Cart = function(){

    var itemCookieName = 'items',
        cookie = new Cookie();

    var internalElements = {
        'cartPreview' : {
            'counter'   : $("#cart-preview-items-count"),
            'price'     : $("#cart-preview-total-price"),
            'link'      : $("#shopping-cart-link-expand")
        },
        'cartView' : {
            'template'       : $("table#cart-items-list"),
            'templateBox'    : $("#cart-items-list tbody"),
            'templateSource' : $("#cart-preview-item"),
	        'itemExpression' : '.cart-item-{id}',
	        'showMoreBlock'  : $('#show-more'),
	        'showMoreCount'  : $('#show-more-count')
        },
        'ajaxUrl'   : '/ajax/getItem',
        'notifyBlock': '#add-to-cart-notify',
	    'maxItemsInCart' : 4
        /*,
        'manageElements': {
            'addToCartClass'        : '.add-to-cart',
            'removeFromCartClass'   : '.remove-from-cart',
            'limitedClass'          : '.limited',
            'emptyCartClass'        : '.empty-cart'
        }*/
    };

	var notify = function(itemId) {
		$(internalElements.notifyBlock)
			.clone()
			.appendTo('body')
			.show()
			.fadeOut(2000, function() {$(this).remove();});
	};

	var number_format = function ( number, decimals, dec_point, thousands_sep ) {
		var i, j, kw, kd, km;

		if( isNaN(decimals = Math.abs(decimals)) ){
			decimals = 2;
		}
		if( dec_point == undefined ){
			dec_point = ",";
		}
		if( thousands_sep == undefined ){
			thousands_sep = ".";
		}

		i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

		if( (j = i.length) > 3 ){
			j = j % 3;
		} else{
			j = 0;
		}

		km = (j ? i.substr(0, j) + thousands_sep : "");
		kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);

		kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");

		return km + kw + kd;
	};

	var addItemToCookie = function(itemCookieName, itemId, limit) {
        var itemId = itemId|0,
            cookieValue = cookie.getCookie(itemCookieName),
            items = cookieValue === '' || !cookieValue.length ? [] : jQuery.parseJSON(cookieValue),
	        i = 0;

        if(itemId > 0 && jQuery.isArray(items)) {
	        for(; i<limit; i++) {
		        items.push('' + itemId);
	        }
        }

        cookie.setCookie(itemCookieName, JSON.stringify(items));
    };

    var removeItemFromCookie = function(itemCookieName, itemId, limit) {
        var itemId = itemId|0,
            currentItem = 0,
            cookieValue = cookie.getCookie(itemCookieName),
            items = cookieValue === '' || !cookieValue.length ? [] : jQuery.parseJSON(cookieValue),
            itemsCount = items.length,
	        limit = limit || 1,
            i = 0;

        if(itemId > 0 && jQuery.isArray(items)) {

            for(; i<itemsCount; i++) {

	            if(limit === 0) {
		            break;
	            }

                currentItem = items[i]|0;
                if(currentItem === itemId) {
                    items.splice(i, 1);
	                limit--;
                    i--;
                }
            }

            cookie.setCookie(itemCookieName, JSON.stringify(items));
        }

    };

    var emptyItemsFromCookie = function(itemCookieName) {
        cookie.deleteCookie(itemCookieName);
    };

    var getTotalItemsCount = function(itemCookieName) {
        var cookieValue = cookie.getCookie(itemCookieName),
            items = cookieValue === '' || !cookieValue.length ? [] : jQuery.parseJSON(cookieValue),
            counter = 0;

        if(jQuery.isArray(items)) {
            counter = parseInt(items.length, 10);
        }

        return counter;

    };

    var getTotalItemCount = function(itemCookieName, itemId) {
        var itemId = itemId|0,
            currentItem = 0,
            cookieValue = cookie.getCookie(itemCookieName),
            items = cookieValue === '' || !cookieValue.length ? [] : jQuery.parseJSON(cookieValue),
            itemsCount = items.length,
            i = 0,
            counter = 0;

        if(itemId > 0 && jQuery.isArray(items)) {
            for(;i<itemsCount;i++) {
	            currentItem = items[i]|0;

                if(currentItem === itemId) {
                    counter++;
                }
            }
        }

        return counter;
    };

    var updateTemplate = function(itemId, operationType, limit) {
        var itemCount = getTotalItemCount(itemCookieName, itemId)|0;

	    if(operationType === 'empty') {
		    emptyTemplate();
	    } else if(itemCount === 0) {
	        updateItemInTemplate(itemId, operationType, limit);
        } else if(itemCount === 1 && operationType === 'add') {
            addItemToTemplate(itemId, limit);
        } else {
            updateItemInTemplate(itemId, operationType, limit);
        }

    };

	var checkItemsOverflow = function() {
		var $itemsInCart = internalElements.cartView.templateBox.find('tr'),
			$showMoreBlock = internalElements.cartView.showMoreBlock,
			$showMoreCountBlock = internalElements.cartView.showMoreCount,
			itemsLength = $itemsInCart.length|0,
			maxLength = internalElements.maxItemsInCart|0;

		$.each($itemsInCart, function(index, value) {
			var $element = $(this);

			if(index >= maxLength) {
				$element.hide();
			} else {
				$element.show();
			}
		});

		if(itemsLength > maxLength) {
			$showMoreBlock.show();
		} else {
			$showMoreBlock.hide();
		}

		$showMoreCountBlock.text(itemsLength);
	};

    var addItemToTemplate = function(itemId, limit) {
        var cartView = internalElements.cartView;

	    $.ajax({
		    type    : 'get',
		    url     : internalElements.ajaxUrl,
		    data    : {'id': itemId},
		    success : function(data) {
			    cartView.templateSource
					    .tmpl(jQuery.parseJSON(data))
					    .appendTo(cartView.templateBox);

			    updateItemInTemplate(itemId, 'add', limit);
		    }
	    });
    };


    var emptyTemplate = function() {
	    var templateBox = internalElements.cartView.templateBox;

	    templateBox.empty();console.log($("#shopping-cart-table > tbody"));
	    $("#shopping-cart-table > tbody").empty();

	    updateTotals(0.0, 0);

	    window.location.reload();
    };

    var updateItemInTemplate = function(itemId, operationType, limit) {
	    var item = getItemBoxFromTemplate(itemId),
		    countBox = item.find('.cart-preview-number'),
		    totalCountBox = internalElements.cartPreview.counter,
		    priceBox = item.find('.item-price'),
		    totalItemPriceBox = item.find('.total-item-price'),
		    totalPriceBox = $("#hidden-total-price"),
		    itemPrice = priceBox.val()|0,
		    itemCount = countBox.val()|0,
		    totalPrice = totalPriceBox.val()| 0,
		    totalCount = totalCountBox.text()|0;

		switch(operationType) {
			case 'add':
				itemCount += limit;
				totalCount += limit;
				totalPrice += itemPrice * limit;
				break;
			case 'remove':
				totalCount = limit > itemCount ? totalCount - itemCount : totalCount - limit;
				totalPrice = limit > itemCount ? totalPrice - (itemPrice * itemCount) : totalPrice - (itemPrice * limit);
				itemCount = limit > itemCount ? 0 : itemCount - limit;
				break;
		}

	    if(itemCount <= 0 && operationType === 'remove') {
		    item.fadeOut(500, function() {
			    $(this).remove();
			    checkItemsOverflow();
		    });
	    }

	    totalItemPriceBox.text(number_format(itemCount * itemPrice, 0, '.', ' '));

	    countBox.each(function() {
		    $(this).val(itemCount)
	    });

	    totalPriceBox.val(totalPrice);

	    updateTotals(totalPrice, totalCount);

	    checkItemsOverflow();
    };

	var updateTotals = function(totalPrice, totalCount) {
		var totalHeaderPriceBox = internalElements.cartPreview.price,
			totalBodyPriceBox = $("#cart-preview-total"),
			totalCountBox = internalElements.cartPreview.counter

		totalCountBox.text(totalCount);
		totalHeaderPriceBox.text(number_format(totalPrice, 0, '.', ' '));
		totalBodyPriceBox.text(number_format(totalPrice, 0, '.', ' '));
		$("#cart-result-sum-number").text(number_format(totalPrice, 0, '.', ' '));
	};

	var getItemBoxFromTemplate = function(itemId) {
		var item = $(internalElements.cartView
									 .itemExpression
									 .replace('{id}', itemId)
		);

		return item;
	};

    this.add = function(itemId, limit) {
	    var limit = limit || 0;

        addItemToCookie(itemCookieName, itemId, limit);
        updateTemplate(itemId, 'add', limit);

	    notify(itemId);

        return false;
    };

    this.remove = function(itemId, limit) {
	    var limit = limit || 0;

        removeItemFromCookie(itemCookieName, itemId, limit);

	    updateTemplate(itemId, 'remove', limit);

	    return false;
    };

    this.empty = function() {
        emptyItemsFromCookie(itemCookieName);
	    updateTemplate(0, 'empty', 0);

	    return false;
    };

    this.getTotalCount = function() {
        return getTotalItemsCount(itemCookieName);
    };

    this.init = function() {
        var cartPreviewLink = internalElements.cartPreview.link,
            manageElements = internalElements.manageElements;

        cartPreviewLink.click(function() {
            var template = internalElements.cartView.template;

            template.toggle('blind', {}, 500);

            return false;
        });
    };
};

var UI = {};

UI.tree = (function() {
    var a = {
            'final'     :   'a.final',
            'branch'    :   'a.branch',
            'active'    :   'a.active'
        },
        list = {
            'branch'    :   'ul.nested'
        };

    var branchClickTrigger = function() {
        var link = $(this);

        link.next('ul').toggle('slow');
        link.toggleClass('open');

        return false;
    };

    var showActiveNode = function() {
        var activeLink = null;

        if(typeof activeNodeUrl !== 'undefined') {
            activeLink =  $('#' + activeNodeUrl);

            activeLink
                .addClass('active')
                .parents('ul')
                .show();

            activeLink
                .parents('li')
                .children('a')
                .addClass('open');
        }
    };

    var initCallbacks = function() {
        var nestedLists = $(list.branch);

        nestedLists.toggle();

        showActiveNode();



        $(document).on('click', a.branch, branchClickTrigger);
    };

    return {
        init: function() {
            initCallbacks();
        }
    };
}());

UI.flyingMenu = (function(window){
	var $menu = null,
		$horizontalFixer = null,
		topMenuPosition = null;

	var scrollWindowTrigger = function() {
		var offset = topMenuPosition - $(window).scrollTop();

		if(offset <= 0) {
			$menu.addClass('fixed-menu');
			$horizontalFixer.css('left', -$(window).scrollLeft());
		} else {
			$menu.removeClass('fixed-menu');
			$horizontalFixer.css('left', 0);
		}
	};

	return {
		'init': function() {
			$menu = $('#main-menu');
			topMenuPosition = $menu.offset().top|0;
			$horizontalFixer = $('#fix-horizontal-scroll');

			$(window).on('scroll', scrollWindowTrigger);
		}
	};
}(window));

UI.moveElement = function(element, parent) {
	var oldOffset = element.offset();
	element.appendTo(parent);
	var newOffset = element.offset();

	var temp = element.clone().appendTo('body');
	temp    .css('position', 'absolute')
		.css('left', oldOffset.left)
		.css('top', oldOffset.top)
		.css('zIndex', 1000);
	element.hide();
	temp.animate( {'top': newOffset.top, 'left':newOffset.left}, 'slow', function(){
		element.show();
		temp.remove();
	});

	element.remove();
};

(function($, window) {

    var Control = function() {
        this.settings = null;
    };

    Control.prototype = {
        setSettings: function(settings) {
            this.settings = settings;
        },
        getSettings: function() {
            return this.settings;
        },
        getSetting: function(key) {
            return this.settings[key];
        }
    };

	var Publisher = function() {
		this.subscribers = [];
	};

	Publisher.prototype.deliver = function() {
		var count = this.subscribers.length,
			i = 0;

		for(; i< count; i++) {
			this.subscribers[i]();
		}
	};

	Function.prototype.subscribe = function(publisher) {
		publisher.subscribers.push(this);
		return this;
	};

	var Switcher = (function(Control, Publisher){
		var baseControl = new Control(),
			publisher = new Publisher(),
			commonState = null,
			controlKey = 'template',
			controlValue = '';

		var changeControlCallback = function() {
			publisher.deliver();
		};

		var initControl = function() {
			var settings = baseControl.getSettings(),
				grid = settings['grid'],
				list = settings['list'],
				switchers = [grid['element'], list['element']],
				activeClass = settings['active'];

			$(document).on('click', switchers.join(','), function() {
				var i = 0,
					count = switchers.length,
					link = $(this);

				if(!link.has(activeClass)) {
					for(; i<count; i++) {
						if(link.has(switchers[i])) {
							link.addClass(activeClass);
						} else {
							link.removeClass(activeClass);
						}
					}

					changeControlCallback();
				}

				return false;
			});
		};

		var init = function() {
			initControl();
			initState();
		};

		var initState = function() {
			controlValue = baseControl.getSetting('grid')['template'];

			commonState.updateState(controlKey, controlValue);
		};

		var drawControl = function() {

		};

		return {
			init        : init,
			draw        : drawControl,
			setState    : function(state) {
				commonState = state;
			},
			setSettings: function(settings) {
				baseControl.setSettings(settings);

				return this;
			},
			settingKey  : 'switcher',
			onChange    :  publisher
		};

	}(Control, Publisher));

    var Pagination = (function(Control, Publisher){

        var baseControl = new Control(),
	        publisher = new Publisher(),
	        commonState = null,
	        controlKey = 'page',
	        controlValue = 1;

	    var changeControlCallback = function() {
		    publisher.deliver();
	    };

        var initControl = function() {
			var settings = baseControl.getSettings(),
				block = settings['block'],
				element = settings['element'],
				activeClass = settings['active'];

	        $(document).on('click', block + ' > ' + element, function() {
		        var link = $(this),
			        block = link.parent(block);

		        block
			        .children(element)
		            .removeClass(activeClass);

		        link.addClass(activeClass);

		        controlValue = parseInt(link.text(), 10);

		        changeControlCallback();

		        return false;
	        });

	        drawControl();
        };

	    var init = function() {
		    initControl();
		    initState();
	    };

	    var initState = function() {
		    commonState.updateState(controlKey, stateCallback);
	    };

	    var stateCallback = function(items, itemsPerPage) {
			var offset = 0,
				viewItems = [];

		    if(itemsPerPage >= items.length) {
			    viewItems = items;
		    } else {
			    offset = itemsPerPage * (controlValue - 1);

			    viewItems = items.slice(offset, offset + itemsPerPage);
		    }

		    return viewItems;
	    };

        var drawControl = function() {

            var settings = baseControl.getSettings(),
                currentPage = controlValue|0,
                countItems = commonState.getState('count')|0,
                itemsPerPage = commonState.getState('itemsPerPage')|0,
                countPages = Math.ceil(countItems/itemsPerPage),
                i = 1,
                block = $(settings['block']),
                element = $(settings['block'] + '>' + settings['element']),
                active = settings['active'],
	            visiblePart = 3;

            if(currentPage > countPages) {
	            currentPage = controlValue = countPages;
	            commonState.updateState(controlKey, stateCallback);
            }

	        block.empty();

            for(; i < countPages + 1; i++) {
	            if(
		            (i <= visiblePart) ||
		            (Math.abs(i - currentPage) <= visiblePart) ||
		            (countPages - i + 1 <= 3)
	            ) {
		            $(element[0])
			            .clone(false)
			            .removeClass(active)
			            .find('a')
			            .text(i)
			            .parent()
			            .addClass( i === currentPage ? active : '')
			            .appendTo(block);
	            } else {
		            if(i%10) {
			            block.append($(element[0]).clone(false).text('.'));
		            }

	            }

            }
        };

        return {
            init        : init,
            draw        : drawControl,
	        setState    : function(state) {
		        commonState = state;

		        return this;
	        },
	        setSettings: function(settings) {
		        baseControl.setSettings(settings);

		        return this;
	        },
	        settingKey  : 'pagination',
	        onChange    :  publisher
        };
    }(Control, Publisher));

    var State = function() {
        var state = {
	        'itemsPerPage': 5
        };

	    this.updateState = function(key, value) {
		    state[key] = value;
	    };

	    this.getState = function(key) {
		    return state[key];
	    }
    };

    var ShowCase = (function(Control){

        var controls = [],
            baseControl = new Control(),
	        commonState = null,
	        controlKey = 'count',
	        controlValue = 0;

        var initControls = function() {
            var countControls = controls.length,
                i = 0;

            for(; i < countControls; i++ ) {
                controls[i].init();

	            draw.subscribe(controls[i].onChange);
            }

        };

	    var initState = function() {
		    controlValue = baseControl.getSetting('items').length;

		    commonState.updateState(controlKey, controlValue);
	    };

        var addControl = function(control, settings) {
            if(settings['enable']) {
                control
                    .setSettings(settings)
                    .setState(commonState);

                controls.push(control);
            }

	        return this;
        };

	    var drawControls = function() {
		    var countControls = controls.length,
			    i = 0;

		    for(; i < countControls; i++ ) {
			    controls[i].draw();
		    }
	    };

	    var drawItems = function() {
		    var items = getViewItems(),
			    block = baseControl.getSetting('block'),
			    template = commonState.getState('template');

		    $("#items-on-page").text(items.length);

		    $(block).empty();
		    $.each(items, function(index, value) {
			    if(!((index+1)%3)) {
				    value['last'] = 'true';
			    }
			    $(template).tmpl(value).appendTo($(block));
		    });

	    };

	    var getViewItems = function() {
		    var settings = baseControl.getSettings(),
			    items = settings['items'],
			    itemsPerPage = commonState.getState('itemsPerPage')|0,
			    viewItems = [];

		    viewItems = commonState.getState('page')(items, itemsPerPage);

		    return viewItems;
	    };

	    var draw = function() {
		    drawControls();
		    drawItems();
	    };

	    var init = function() {
		    initState();
		    initControl();
		    initControls();
		    drawItems();
	    };

	    var initControl = function() {

	    };

        return {
            init: init,
            add : addControl,
            setSettings: function(settings) {
	            baseControl.setSettings(settings);

	            return this;
            },
            setState: function(state) {
	            commonState = state;

	            return this;
            },
	        draw: draw
        };

    }(Control));

    $.fn.showCase = function(options) {
        var defaultSettings = {

            },
            settings = $.extend(defaultSettings, options);

        return this.each(function() {

            ShowCase.setSettings(settings)
	                .setState(new State())
                    .add(Pagination, settings[Pagination.settingKey])
                    .add(Switcher, settings[Switcher.settingKey])
                    .init();
        });
    };

}(jQuery, window));

$(function(){
    window.cart = new Cart();
    cart.init();

    UI.tree.init();
	UI.flyingMenu.init();
});