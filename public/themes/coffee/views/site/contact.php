<h1>Контактная информация</h1>

<div id="contacts">
    <p class="contacts-element">Заказать товар или получить консультацию можно круглосуточно по телефонам: </p>

    <ul class="contacts-element b">
        <li>+375 (29) 137-61-40 </li>
        <li>+375 (33) 612-51-98</li>
        <li>+375 (44) 738-89-60</li>
    </ul>

    <p class="contacts-element">или через интернет: </p>

    <ul class="contacts-element">
        <li>
            <span class="b">Skype:</span> www.bogemia.test
        </li>
        <li>
            <span class="b">E-mail:</span> 21@21vek.by
        </li>
    </ul>

    <p class="contacts-element">
        <span class="b">Онлайн-заказы</span> через сайт 21vek.by принимаются <span class="b">круглосуточно!</span>
    </p>
    <p class="contacts-element">В рабочее время вам перезвонит менеджер для уточнения деталей заказа. </p>

</div>