<h1>Корзина</h1>

<div id="shopping-cart-page">
    <table id="shopping-cart-table">
        <thead>
            <tr>
                <th></th>
                <th>Наименование товара</th>
                <th>Цена за единицу, бел.руб.</th>
                <th>Количество единиц</th>
                <th>Итого, бел.руб.</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach(Yii::app()->params['cart']->getItems() as $item) : ?>
            <tr class="cart-item-<?php echo $item->id; ?>">
                <td>
                    <img src="<?php echo $item->getImage();?>" />
                </td>
                <td>
                    <a class="cart-title" href="<?php echo $item->getUrl(true);?>"><?php echo $item->getTitle(); ?></a>
                </td>
                <td class="cart-price"><?php echo $item->getPrice(true); ?></td>
                <td>
                    <a href="javascript: void(0)" onclick="cart.remove(<?php echo $item->getId(); ?>, 1);" class="minus-item">&#8211;</a>
                    <input type="text" class="cart-preview-number" value="<?php echo $item->getCount(); ?>" />
                    <a href="javascript: void(0)" onclick="cart.add(<?php echo $item->getId(); ?>, 1);" class="plus-item">+</a>
                </td>
                <td class="cart-price total-item-price"><?php echo $item->getTotalPrice(true); ?></td>
                <td>
                    <a href="javascript: void(0)" onclick="cart.remove(<?php echo $item->getId(); ?>, 1000);" class="cart-delete"></a>
                </td>
            </tr>
        <?php endforeach;?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2">
                    <span id="cart-result-sum-label">ИТОГО: &nbsp;</span>
                    <span id="cart-result-sum-number"><?php echo $cart->getTotalPrice(true); ?></span>
                </td>
                <td></td>
                <td colspan="3">
                    <a id="empty-cart" href="javascript: void(0)" onclick="cart.empty();">ОЧИСТИТЬ КОРЗИНУ</a>
                </td>
            </tr>
        </tfoot>
    </table>
</div>

<h2>Заполните данные для заказа</h2>

<div id="checkout-info">

    <?php if(Yii::app()->user->hasFlash('error')):?>
        <div class="form-row headerErrorMessage">
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
    <?php endif; ?>

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id'=>'checkout-form',
        'enableAjaxValidation'=>true,
        'enableClientValidation'=>true,
        'focus'=>array($model,'name'),
    )); ?>

    <div class="form-row">
        <?php echo $form->radioButtonList($model, 'delivery', array(
            'courier'   =>  'Доставка курьером по Минску',
            'mail'      =>  'Доставка почтой по РБ',
        ), array(
            'template'  =>  '<div class="form-row">
                                {input}
                                {label}
                            </div>',
            'separator' =>  ''
        )); ?>
    </div>

    <div class="form-row">
        <?php echo $form->labelEx($model, 'name', array('class' =>  'block')); ?>
        <?php echo $form->textField($model, 'name', array('class' =>  'input input-text')); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>

    <div class="form-row">
        <?php echo $form->labelEx($model, 'email', array('class' =>  'block')); ?>
        <?php echo $form->textField($model, 'email', array('class' =>  'input input-text')); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>

    <div class="form-row">
        <?php echo $form->labelEx($model, 'phone', array('class' =>  'block')); ?>
        <?php echo $form->textField($model, 'phone', array('class' =>  'input input-text')); ?>
        <?php echo $form->error($model, 'phone'); ?>
    </div>

    <div class="form-row">
        <?php echo $form->labelEx($model, 'address', array('class' =>  'block')); ?>
        <?php echo $form->textArea($model, 'address', array('rows' =>  5, 'class' => 'textarea')); ?>
        <?php echo $form->error($model, 'address'); ?>
    </div>

    <button class="btn submit" id="checkout-button" type="submit">ЗАКАЗАТЬ</button>

    <?php $this->endWidget(); ?>
</div>