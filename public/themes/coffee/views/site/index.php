<h1>Интернет-магазин кофе для вас</h1>

<?php if(isset($hot)) : ?>
    <p class="index-items">Лидеры продаж</p>
    <?php $renderer->renderPartial('_coffee_list', array('items' => $hot, 'type' => 'list')); ?>
<?php endif; ?>

<?php if(isset($sale)) : ?>
    <p class="index-items">Товары со скидкой</p>
    <?php $renderer->renderPartial('_coffee_list', array('items' => $sale, 'type' => 'list')); ?>
<?php endif; ?>
