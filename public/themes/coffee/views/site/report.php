<h1>Спасибо за ваш заказ!</h1>

<div id="report-page">
    <p>Ваш заказ принят на обработку. Наш менеджер свяжется с вами в ближайшее время.</p>

    <table id="order-report-info">
        <tbody>
        <?php foreach($report->getOrder() as $name=>$value) : ?>
            <tr>
                <td><?php echo $name; ?></td>
                <td class="report-value"><?php echo $value; ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <table id="report-table" cellpadding="0" cellspacing="0">
        <tbody>
            <?php foreach($report->getItems() as $key => $item) : ?>
            <tr <?php if($key === 0) : ?>  class="report-total-row" <?php endif;?>>
                <td class="report-item-title"><?php echo $item['title']; ?></td>
                <td class="report-item-space"><?php echo $item['count']; ?> x</td>
                <td class="report-item-price"><?php echo $item['price']; ?></td>
            </tr>
            <?php endforeach; ?>
            <tr class="report-total-row">
                <td class="report-total-text">Промежуточный итог: </td>
                <td class="report-total-space">&nbsp;</td>
                <td class="report-total-price"><?php echo $report->getSubTotalPrice();?> бел. руб.</td>
            </tr>
            <tr class="report-total-row">
                <td class="report-total-text">Итоговая цена: </td>
                <td class="report-total-space">&nbsp;</td>
                <td class="report-total-price"><?php echo $report->getTotalPrice();?> бел. руб.</td>
            </tr>
        </tbody>
    </table>
</div>
