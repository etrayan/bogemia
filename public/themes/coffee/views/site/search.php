<h1>Результаты поиска по запросу: "<?php echo $query; ?>"</h1>

<?php if(isset($result['items'])) : ?>
    <p class="result-title">Результаты по товарам</p>
    <?php echo $result['items'];?>
<?php endif; ?>

<?php if(isset($result['articles'])) : ?>
    <p class="result-title">Результаты по статьям</p>
    <?php echo $result['articles'];?>
<?php endif; ?>

<?php if(isset($result['brands'])) : ?>
    <p class="result-title">Результаты по брендам</p>
    <?php echo $result['brands'];?>
<?php endif; ?>