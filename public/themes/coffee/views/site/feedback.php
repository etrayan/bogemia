<h1>Узнайте интересующую вас информацию</h1>

<div id="feedback-page">

    <?php if(Yii::app()->user->hasFlash('error')):?>
        <div class="form-row headerErrorMessage">
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
    <?php endif; ?>

    <?php if(Yii::app()->user->hasFlash('success')):?>
        <div class="form-row headerSuccessMessage">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id'=>'feedback-form',
        'enableAjaxValidation'=>true,
        'enableClientValidation'=>true,
        'focus'=>array($model,'title'),
    )); ?>

    <div class="form-row">
        <?php echo $form->labelEx($model, 'title', array('class' =>  'block')); ?>
        <?php echo $form->textField($model, 'title', array('class' =>  'input input-text')); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>

    <div class="form-row">
        <?php echo $form->labelEx($model, 'email', array('class' =>  'block')); ?>
        <?php echo $form->textField($model, 'email', array('class' =>  'input input-text')); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>

    <div class="form-row">
        <?php echo $form->labelEx($model, 'phone', array('class' =>  'block')); ?>
        <?php echo $form->textField($model, 'phone', array('class' =>  'input input-text')); ?>
        <?php echo $form->error($model, 'phone'); ?>
    </div>

    <div class="form-row">
        <?php echo $form->labelEx($model, 'body', array('class' =>  'block')); ?>
        <?php echo $form->textArea($model, 'body', array('rows' =>  5, 'class' => 'textarea')); ?>
        <?php echo $form->error($model, 'body'); ?>
    </div>

    <button class="btn submit" id="checkout-button" type="submit">ОТПРАВИТЬ</button>

    <?php $this->endWidget(); ?>
</div>