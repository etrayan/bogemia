<h1>Каталог</h1>

<div id="catalog-tools">
    <div id="display-tools" class="fl">
        <a href="javascript: void(0)" class="list-tool"></a>
        <a href="javascript: void(0)" class="grid-tool active"></a>
    </div>
    <div id="sorting-tools" class="fr">
        <a href="javascript: void(0)">
            Сортировать по
        </a>
        <select id="sort-by-list">
            <option>Name</option>
            <option>Price</option>
        </select>
    </div>
    <div class="cb"></div>
</div>

<div id="pagination-tools">
    <p class="fl" id="visible-items">6 товаров (из 20)</p>

    <div id="pagination" class="fr">
        <a href="javascript: void(0)">1</a>
        <a href="javascript: void(0)">2</a>
        <a href="javascript: void(0)">3</a>
    </div>
    <div class="cb"></div>
</div>

<ul id="items-list-grid">
    <?php for($i=1;$i<7;$i++) : ?>
        <li class="<?php echo (($i+1)%3) ? '' : 'last-child-right';?>">
            <a class="item-list-title" href="<?php echo Yii::app()->createUrl('site/tovar');?>">FOLGERS BLACK SILK GROUND COFFEE</a>
            <div>
                <img class="item-list-image" src="<?php echo Yii::app()->params->images['relative'];?>items/1.png" alt="Lavazza кофе зеленые зерна" />
            </div>
            <div class="item-list-description">
                <p class="item-list-price">200 000 бел.руб.</p>
                <button class="btn item-list-add-cart" onclick="cart.add(<?php echo $i;?>)">В КОРЗИНУ</button>
            </div>
        </li>
    <?php endfor; ?>
</ul>
<div class="cb"></div>