<h1>Ваша корзина пуста</h1>

<div id="shopping-cart-page">
    <p class="empty-cart">У вас нету ни одного товара в корзине</p>
    <p class="empty-cart">Нажмите <a href="<?php echo Yii::app()->createUrl('site/index');?>">сюда</a> чтобы продолжить покупки</p>
</div>