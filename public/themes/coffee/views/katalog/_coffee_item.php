<li class="<?php echo (($index+1)%3) ? '' : 'last-child-right';?>">
    <a class="item-list-title" href="<?php echo $data->getUrl(true);?>"><?php echo $data->getTitle();?></a>
    <div>
        <a class="item-list-title" href="<?php echo $data->getUrl(true);?>">
            <img class="item-list-image image-<?php echo $data->getId();?>" src="<?php echo $data->getImage(true);;?>" alt="<?php echo $data->getTitle();?>" />
        </a>
    </div>
    <div class="item-list-description">
        <?php if($data->isBrand()) : ?>
            <a class="btn item-list-add-cart" href="<?php echo $data->getUrl(true);?>">ЧИТАТЬ</a>
        <?php else : ?>
            <p class="item-list-price"><?php echo $data->getPrice(true);?> бел.руб.</p>
            <button class="btn item-list-add-cart" onclick="cart.add(<?php echo $data->getId();?>, 1)">В КОРЗИНУ</button>
        <?php endif; ?>
    </div>
</li>