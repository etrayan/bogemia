<h1><?php echo $title; ?></h1>

<div id="catalog-tools">
<!--    <div id="display-tools" class="fl">-->
<!--        <a href="javascript: void(0)" class="list-tool"></a>-->
<!--        <a href="javascript: void(0)" class="grid-tool active"></a>-->
<!--    </div>-->
<!--    <div id="sorting-tools" class="fr">-->
<!--        <a href="javascript: void(0)">-->
<!--            Сортировать по-->
<!--        </a>-->
<!--        <select id="sort-by-list">-->
<!--            <option>Name</option>-->
<!--            <option>Price</option>-->
<!--        </select>-->
<!--    </div>-->
    <div class="cb"></div>
</div>

<div id="pagination-tools">
    <p class="fl" id="visible-items"><span id="items-on-page"><?php echo count($items);?></span> <span id="items-noun">элемента</span> (из <?php echo count($items); ?>)</p>

    <ul class="pagination" id="pagination">
        <li class="page selected">
            <a href="javascript: void(0)"></a>
        </li>
    </ul>
<!--    <div id="pagination" class="fr">-->
<!---->
<!--    </div>-->
    <div class="cb"></div>
</div>

<ul id="items-list-grid">
    <?php foreach($items as $key=>$item) : ?>
        <li class="<?php echo (($key+1)%3) ? '' : 'last-child-right';?>">
            <a class="item-list-title" href="<?php echo $item['url'];?>"><?php echo $item['title'];?></a>
            <div>
                <a class="item-list-title" href="<?php echo $item['url'];?>">
                    <img class="item-list-image image-<?php echo $item['id'];?>" src="<?php echo $item['image'];?>" alt="<?php echo $item['title'];?>" />
                </a>
            </div>
            <div class="item-list-description">
                <?php if(isset($isBrand) && $isBrand) : ?>
                    <a class="btn item-list-add-cart" href="<?php echo $item['url'];?>">ЧИТАТЬ</a>
                <?php else : ?>
                    <p class="item-list-price"><?php echo $item['price'];?> бел.руб.</p>
                    <button class="btn item-list-add-cart" onclick="cart.add(<?php echo $item['id'];?>, 1)">В КОРЗИНУ</button>
                <?php endif; ?>

            </div>
        </li>
    <?php endforeach; ?>
</ul>
<div class="cb"></div>
<script>
    var activeNodeUrl = '<?php echo isset($url) ? $url : ''; ?>',
        items = <?php echo $json; ?>;
</script>

<div id="items-grid-template" style="display: none;">
    <li class="{%if last%} last-child-right {%/if%}">
        <a class="item-list-title" href="${url}">${title}</a>
        <div>
            <a href="${url}">
                <img class="item-list-image image-${id}" src="${image}" alt="${title}" />
            </a>
        </div>
        <div class="item-list-description">
            <?php if(isset($isBrand) && $isBrand) : ?>
                <a class="btn item-list-add-cart" href="${url}">ЧИТАТЬ</a>
            <?php else :?>
                <p class="item-list-price">${price} бел.руб.</p>
                <button class="btn item-list-add-cart" onclick="cart.add(${id}, 1)">В КОРЗИНУ</button>
            <?php endif;?>
        </div>
    </li>
</div>

<div id="items-list-template" style="display: none;">
    <li class="{%if last%} last-child-right {%/if%}">
        <a class="item-list-title" href="${url}">${title}</a>
        <div>
            <a href="${url}">
                <img class="item-list-image" src="${image}" alt="${title}" />
            </a>
        </div>
        <div class="item-list-description">
            <?php if(isset($isBrand) && $isBrand) : ?>
<!--                <p class="item-list-price">${price} бел.руб.</p>-->
                <a class="btn item-list-add-cart" href="${url}">ЧИТАТЬ</a>
            <?php else :?>
                <p class="item-list-price">${price} бел.руб.</p>
                <button class="btn item-list-add-cart" onclick="cart.add(${id}, 1)">В КОРЗИНУ</button>
            <?php endif;?>
        </div>
    </li>
</div>
<script>
    $(function() {
        $('ul#items-list-grid').showCase({
            'items': items,
            'block' : 'ul#items-list-grid',
            'switcher': {
                enable: true,
                grid: {
                    element: '.grid-tool',
                    template: '#items-grid-template'
                },
                list: {
                    element: '.list-tool',
                    template: '#items-grid-template'
                },
                active: 'active'
            },
            'pagination': {
                enable: true,
                block: '#pagination',
                element: 'li',
                active: 'selected'
            }
        });
    });
</script>