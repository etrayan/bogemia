<?php if($type === 'widget') : ?>
    <?php
    $this->widget('zii.widgets.CListView', array(
        'dataProvider'  =>  $items,
        'itemView'  =>  '_coffee_item',
        'template'=>'{pager} <ul class="items-list-grid"> {items} </ul> {pager}',
        'pager' =>  array(
            'cssFile'=>false,
            'htmlOptions' => array(
                'class' =>  'pagination'
            ),
            'header' => 'Страницы: ',
            'firstPageLabel'=>'<<',
            'prevPageLabel'=>'<',
            'nextPageLabel'=>'>',
            'lastPageLabel'=>'>>',
        )
    ));
    ?>

<?php elseif($type === 'list') : ?>
<ul class="items-list-grid">
    <?php foreach($items as $key => $item) : ?>
        <?php echo $this->render('_coffee_item', array('data' => $item, 'index' => $key));?>
    <?php endforeach; ?>
</ul>
<?php endif;?>
