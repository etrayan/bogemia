<h1><?php echo $title; ?></h1>

<?php
$this->renderPartial('_coffee_list', array(
    'items'     =>  $items,
    'type'      =>  $type
));
?>
<script>
    var activeNodeUrl = '<?php echo isset($url) ? $url : ''; ?>';
</script>