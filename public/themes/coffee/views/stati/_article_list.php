<?php if($type === 'widget') : ?>
    <?php
    $this->widget('zii.widgets.CListView', array(
        'dataProvider'  =>  $articles,
        'itemView'  =>  '_article_item',
        'template'=>'{pager} <div id="articles-page" class="page-box"><ul id="articles-list"> {items} </ul></div> {pager}',
        'pager' =>  array(
            'cssFile'=>false,
            'htmlOptions' => array(
                'class' =>  'pagination'
            ),
            'header' => 'Страницы: ',
            'firstPageLabel'=>'<<',  //fill in the following as you want
            'prevPageLabel'=>'<',
            'nextPageLabel'=>'>',
            'lastPageLabel'=>'>>',
        )
    ));
    ?>
<?php elseif($type === 'list') : ?>
<ul id="articles-list">
    <?php foreach($articles as $article) : ?>
        <?php echo $this->renderPartial('_article_item', array('data' => $article)); ?>
    <?php endforeach;?>
</ul>
<?php endif;?>
