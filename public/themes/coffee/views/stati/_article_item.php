<li class="article">
    <a class="article-title" href="<?php echo $data->getUrl(true); ?>"><?php echo $data->title; ?></a>
    <p class="article-date"><?php echo $data->getDate(true); ?></p>
    <div class="article-body">
        <img src="<?php echo $data->getImage(true); ?>" alt="<?php echo $data->title; ?>" class="article-image fl" />
        <p class="article-description">
            <?php echo $data->description; ?>
        </p>
        <a class="read-article btn" href="<?php echo $data->getUrl(true); ?>">ЧИТАТЬ</a>
    </div>
</li>