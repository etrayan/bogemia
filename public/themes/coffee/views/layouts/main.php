<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8" />

	<?php
		Yii::app()->clientScript->registerCssFile('/css/site.css');
	?>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700&amp;subset=latin,cyrillic,cyrillic-ext' rel='stylesheet' type='text/css' />

    <!-- include js files -->
    <?php
        Yii::app()->clientScript->registerCoreScript('jquery');
        Yii::app()->clientScript->registerCoreScript('jqueryui');
        Yii::app()->clientScript->registerCoreScript('site.js');
        Yii::app()->clientScript->registerCoreScript('jquery.templates');
    ?>

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
<div id="page">
    <div id="header">
        <?php require_once "header.php"; ?>
    </div>
    <div id="fix-horizontal-scroll">
        <div id="main-menu">
            <?php require_once "main_menu.php"; ?>
        </div>
    </div>

    <div id="breadcrumbs">
        <?php require_once "breadcrumbs.php"; ?>
    </div>

    <div id="content-block">
        <div id="categories" class="fl">
            <?php $this->widget('application.components.widgets.tree.CategoryTree'); ?>
        </div>
        <div id="main-content" class="fl">
            <?php echo $content; ?>
        </div>
        <div class="cb"></div>
    </div>

    <div id="footer">
        <?php require_once "footer.php"; ?>
    </div>
</div>
</body>
</html>
