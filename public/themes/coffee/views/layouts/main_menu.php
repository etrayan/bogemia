<?php
$this->widget('zii.widgets.CMenu',array(
    'items'=>array(
        array('label'=>'ГЛАВНАЯ', 'url'=>array('/site/index'), 'linkOptions'=>array('class'=>'menu-home menu-element')),
        array('label'=>'КАТАЛОГ', 'url'=>array('/katalog/index'), 'linkOptions'=>array('class'=>'menu-element')),
        array('label'=>'СТАТЬИ', 'url'=>array('/stati/index'), 'linkOptions'=>array('class'=>'menu-element')),
        array('label'=>'БРЕНДЫ', 'url'=>array('/brendyi/index'), 'linkOptions'=>array('class'=>'menu-element')),
        array('label'=>'КОНТАКТЫ', 'url'=>array('/site/contact'), 'linkOptions'=>array('class'=>'menu-element')),
        array('label'=>'ОБРАТНАЯ СВЯЗЬ', 'url'=>array('/site/feedback'), 'linkOptions'=>array('class'=>'menu-element'))
    )
));
?>
<div class="fr">
    <?php $this->widget('application.components.widgets.cart.ShoppingCart'); ?>
</div>
<div class="cb"></div>