<div id="logo" class="fl">
    <a href="/">
        <img src="<?php echo Yii::app()->params->images['relative'];?>/site/logo.png" alt="Логотип bogemia.by" />
    </a>
</div>

<div id="header-content" class="fr">
    <div id="header-contacts" class="fl">
        <ul id="header-contacts-list">
            <li class="phone">
                8 (033) 612 51 98
            </li>
            <li class="phone">
                8 (044) 738 89 60
            </li>
            <li class="phone">
                8 (029) 137 61 40
            </li>
            <li class="skype">
                azazelo13.edik
            </li>
            <li class="phone">
                8 (029) 137 61 40
            </li>
            <li class="phone">
                8 (029) 137 61 40
            </li>
        </ul>
    </div>
    <div id="header-controls" class="fr">
        <?php
        echo CHtml::beginForm($this->createUrl('site/search'), 'post', array(
                'id'    =>  'search-form',
                'name'  =>  'search-from'
            ));

        echo CHtml::textField('search-field', '', array(
                'id'    =>  'search-field'
            ));

        echo CHtml::button('ПОИСК', array(
                'class' =>  'btn',
                'id'    =>  'search-button',
                'type'  =>  'submit'
            ));

        echo CHtml::endForm();
        ?>
<!--        <form id="search-form" method="post" name="search-form" action="--><?php //; ?><!--">-->
<!--            <input type="text" id="search-field" name="search-field" />-->
<!--            <button type="submit" class="btn" id="search-button">ПОИСК</button>-->
<!--        </form>-->
    </div>
</div>

<div class="cb"></div>

<script id="cart-preview-item" type="text/x-jquery-tmpl">
    <tr class="cart-item-${id}">
        <td class="cart-preview-title">
            <a class="cart-title-preview" href="${url}">${title}</a>
        </td>
        <td class="cart-preview-count">
            <a href="javascript: void(0)" class="minus-item" onclick="cart.remove(${id}, 1);">&#8211;</a>
            <input type="text" class="cart-preview-number" value="0" />
            <input type="hidden" class="item-price" value="${price}" />
            <a href="javascript: void(0)" class="plus-item" onclick="cart.add(${id}, 1);">+</a>
        </td>
        <td class="cart-preview-delete">
            <a href="javascript: void(0)" onclick="cart.remove(${id}, 1000);">
                <img src="<?php echo Yii::app()->params->images['relative'];?>/site/delete.png" alt="${title}" />
            </a>
        </td>
    </tr>
</script>
