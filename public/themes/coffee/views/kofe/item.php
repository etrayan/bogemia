<div id="item-page">
    <div id="item-image-box" class="fl">
        <img src="<?php echo $item->getImage(true); ?>" style="width: 100%;" alt="<?php echo $item->getTitle(); ?>" />
    </div>
    <div id="item-description-box" class="fl">
        <div id="item-cart-info">
            <?php if(!$isBrand) : ?>
            <p id="item-price" class="fl"><?php echo $item->getPrice(true); ?> бел.руб.</p>
            <button id="item-add-cart" class="btn fr" onclick="cart.add(<?php echo $item->getId(); ?>, 1)">В КОРЗИНУ</button>
            <?php endif; ?>
            <div class="cb"></div>
        </div>
        <div id="item-info-box">
            <p id="item-title"><?php echo $item->getTitle(); ?></p>
            <p id="item-description"><?php echo $item->getDescription(); ?></p>
        </div>
    </div>
    <div class="cb"></div>
</div>

<!--<div id="item-relation-items">-->
<!--    <h2>Сопутствующие товары</h2>-->
<!--    <ul id="item-relation-items-list">-->
<!--        --><?php //for($i=0;$i<10;$i++) : ?>
<!--            <li>-->
<!--                <a class="item-list-title" href="--><?php //echo Yii::app()->createUrl('site/tovar');?><!--">FOLGERS BLACK SILK GROUND COFFEE</a>-->
<!--                <div>-->
<!--                    <img class="item-list-image" src="--><?php //echo Yii::app()->params->images['relative'];?><!--items/2.png" alt="Lavazza кофе зеленые зерна" />-->
<!--                </div>-->
<!--                <div class="item-list-description">-->
<!--                    <p class="item-list-price">200 000 бел.руб.</p>-->
<!--                    <button class="btn item-list-add-cart">В КОРЗИНУ</button>-->
<!--                </div>-->
<!--            </li>-->
<!--        --><?php //endfor; ?>
<!--    </ul>-->
<!--    <div class="clearfix"></div>-->
<!--</div>-->
<!--<script>-->
<!--    $(function(){-->
<!--        $("#item-relation-items-list").carouFredSel({-->
<!--            width: 735,-->
<!--            height: 354,-->
<!--            items: {-->
<!--                visible: 3,-->
<!--                width: 232,-->
<!--                height: 354-->
<!--            },-->
<!--            scroll: {-->
<!--                items: 1-->
<!--            },-->
<!--            auto: {-->
<!--                play: true-->
<!--            }-->
<!--        });-->
<!--    });-->
<!--</script>-->