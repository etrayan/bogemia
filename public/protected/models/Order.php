<?php

/**
 * This is the model class for table "order".
 *
 * The followings are the available columns in table 'order':
 * @property integer $id
 * @property string $date
 * @property integer $price
 * @property string $delivery
 * @property string $phone
 * @property string $email
 * @property string $name
 * @property string $address
 * @property string $status
 *
 * The followings are the available model relations:
 * @property Item[] $items
 */
class Order extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('price, phone, name', 'required'),
			array('price', 'numerical', 'integerOnly'=>true),
			array('delivery', 'length', 'max'=>7),
			array('phone', 'length', 'max'=>45),
			array('email, name', 'length', 'max'=>100),
			array('status', 'length', 'max'=>9),
			array('address', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, date, price, delivery, phone, email, name, address, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'items' => array(self::MANY_MANY, 'Item', 'order_has_item(order_id, item_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'id',
			'date' => 'Дата заказа',
			'price' => 'Price',
			'delivery' => 'Доставка',
			'phone' => 'Телефон',
			'email' => 'Email',
			'name' => 'Имя',
			'address' => 'Адрес доставки',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('delivery',$this->delivery,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function afterSave()
    {
        $now = new DateTime();

        $this->date = $now->format('Y-m-d');

        $this->notify();
    }

    public function bindItemsFromCart(CartPreview $cart) {
        $items = $cart->getItems();
        $ohi = null;

        foreach($items as $item) {
            $ohi = new OrderItem();

            $ohi->order_id = $this->id;
            $ohi->item_id = $item['id'];

            $ohi->save();
        }
    }

    private function notify()
    {

    }
}