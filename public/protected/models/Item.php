<?php

/**
 * This is the model class for table "item".
 *
 * The followings are the available columns in table 'item':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property integer $price
 * @property string $url
 * @property string $page_title
 * @property string $page_description
 * @property string $page_keywords
 * @property integer $page_index
 *
 * The followings are the available model relations:
 * @property Category[] $categories
 * @property Comment[] $comments
 * @property Order[] $orders
 * @property RelatedItem[] $relatedItems
 * @property RelatedItem[] $relatedItems1
 */
class Item extends CActiveRecord implements SearchActiveRecord, SEOInterface
{
	use Search;
	use SEO;

    private $count = 0;
    private $totalPrice = 0.0;

    protected $templateFile = '_coffee_list';
	protected $renderObjectName = 'items';
	protected $controller = 'KatalogController';
	protected $uniqueSearchKey = 'items';

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Item the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function behaviors(){
		return array(
			'urlBehavior' => array(
				'class' => 'application.components.behaviors.UrlBehavior',
				'controller' => 'kofe'
			),
			'imageBehavior' => array(
				'class' => 'application.components.behaviors.ImageBehavior',
				'imagesFolder' => 'items'
			)
		);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, description, image', 'required'),
			array('price', 'numerical', 'integerOnly'=>true),
			array('title, image', 'length', 'max'=>100),
            array('url', 'length, page_title, page_keywords', 'max'=>255),
			array('page_description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, description, image, price, url, page_title, page_description, page_keywords, page_index', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categories' => array(self::MANY_MANY, 'Category', 'category_has_item(item_id, category_id)'),
			'comments' => array(self::MANY_MANY, 'Comment', 'item_has_comment(item_id, comment_id)'),
			'orders' => array(self::MANY_MANY, 'Order', 'order_has_item(item_id, order_id)'),
			'relatedItems' => array(self::HAS_MANY, 'RelatedItem', 'parent_id'),
			'relatedItems1' => array(self::HAS_MANY, 'RelatedItem', 'related_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'description' => 'Description',
			'image' => 'Image',
			'price' => 'Price',
            'url' => 'Url',
			'page_title' => 'Page Title',
			'page_description' => 'Page Description',
			'page_keywords' => 'Page Keywords',
			'page_index' => 'Page Index',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('price',$this->price);
        $criteria->compare('url',$this->url,true);
		$criteria->compare('page_title',$this->page_title,true);
		$criteria->compare('page_description',$this->page_description,true);
		$criteria->compare('page_keywords',$this->page_keywords,true);
		$criteria->compare('page_index',$this->page_index);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getItemsByIds($ids = array())
    {
        $items = array();

        $criteria = new CDbCriteria();
        $criteria->select = 'id, title, url, price, image';
        $criteria->addInCondition('id', $ids);

        $items = $this->findAll($criteria);

        return $items;
    }

    public function getItemInfoForAjax($id)
    {
        $item = array();

        $sql = "SELECT id, title, price
                FROM `item`
                WHERE id=:id;";

        $item = Yii::app()->db
                          ->createCommand($sql)
                          ->bindParam(':id', $id)
                          ->queryRow();

        return $item;
    }

    public function setCount($count)
    {
        $this->count = $count;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getPrice($prepared=false)
    {
        if($prepared) {
            return CurrencyHelper::formatBLR($this->price);
        } else {
            return $this->price;
        }
    }

    public function getTotalPrice($prepared=false)
    {
        if($prepared) {
            return CurrencyHelper::formatBLR($this->price * $this->count);
        } else {
            return $this->price * $this->count;
        }
    }

    public function getItemByUrl($url)
    {
        $criteria = new CDbCriteria();

        $criteria->select = 'id, title, url, image, description, price';
        $criteria->condition = 'url=:url';
        $criteria->params = array(
            ':url'  =>  $url
        );

        return $this->find($criteria);
    }

    public function getAllProvider()
    {
        $criteria = new CDbCriteria();
        $criteria->together = false;

        return  new CActiveDataProvider('Item', array(
            'criteria'  =>   $criteria,
            'pagination'=>array(
                'pageSize'  =>  5,
            ),
        ));
    }

    public function getProviderByCategoryUrl($url)
    {
        $criteria = new CDbCriteria();
        $criteria->together = true;
        $criteria->with = 'categories';
        $criteria->condition = 'categories.url=:url';
        $criteria->params = array(
            ':url'  =>  $url
        );

        return  new CActiveDataProvider('Item', array(
            'criteria'  =>   $criteria,
            'pagination'=>array(
                'pageSize'  =>  5,
            ),
        ));
    }

    public function isBrand()
    {
        return false;
    }

    public function getSaleItems()
    {
        $criteria = new CDbCriteria();
        $criteria->limit = 3;
        $criteria->order = 'id ASC';

        return $this->findAll($criteria);
    }

    public function getHotItems()
    {
        $criteria = new CDbCriteria();
        $criteria->limit = 3;
        $criteria->order = 'id DESC';

        return $this->findAll($criteria);
    }
}