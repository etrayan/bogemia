<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property string $id
 * @property string $left
 * @property string $right
 * @property integer $level
 * @property string $title
 * @property integer $page_id
 * @property string $url
 *
 * The followings are the available model relations:
 * @property Page $page
 * @property Item[] $items
 */
class Category extends CActiveRecord
{
    private $rootId     = 1;
    private $brandsId   = 2;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'category';
	}

    public function behaviors()
    {
        return array(
            'NestedSetBehavior'=>array(
                'class'=>'ext.nestedsets.NestedSetBehavior',
                'leftAttribute'=>'left',
                'rightAttribute'=>'right',
                'levelAttribute'=>'level',
                'hasManyRoots'=>false
            ),
	        'urlBehavior' => array(
		        'class' => 'application.components.behaviors.UrlBehavior',
		        'controller' => 'katalog'
	        )
        );
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(/*'left, right, level,*/'title', 'required'),
			array('level, page_id', 'numerical', 'integerOnly'=>true),
			array('left, right', 'length', 'max'=>10),
			array('title', 'length', 'max'=>50),
            array('url', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, left, right, level, title, page_id, url', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'page' => array(self::BELONGS_TO, 'Page', 'page_id'),
			'items' => array(self::MANY_MANY, 'Item', 'category_has_item(category_id, item_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'left' => 'Left',
			'right' => 'Right',
			'level' => 'Level',
			'title' => 'title',
			'page_id' => 'Page',
            'url' => 'Url',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('left',$this->left,true);
		$criteria->compare('right',$this->right,true);
		$criteria->compare('level',$this->level);
		$criteria->compare('title',$this->name,true);
		$criteria->compare('page_id',$this->page_id);
        $criteria->compare('url',$this->url,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getNodeById($id)
    {
        $node = false;

        $brandSql = "SELECT c.left, c.right, c.level
                     FROM `category` AS c
                     WHERE c.id = :id";

        $node = Yii::app()->db
                            ->createCommand($brandSql)
                            ->bindParam("id", $id)
                            ->queryRow();


        if($node === false) {
            throw new CDbException('Не найдены бренды');
        }

        return $node;
    }

    public function getCategoriesAsTree()
    {
        $root = $this->getNodeById($this->rootId);

        $sql = "SELECT c.id, c.title, c.level, c.left, c.right, c.url
                FROM `category` AS c
                WHERE c.left > :left
                AND c.right < :right";

        $categories = Yii::app()->db
                                ->createCommand($sql)
                                ->bindValues(array(
                                    'left'  =>  $root['left'],
                                    'right' =>  $root['right']
                                ))
                                ->queryAll();

        $categoriesTree = $this->categoriesToTree($categories, $root);

        return $categoriesTree;

    }

    private function categoriesToTree($categories, $node)
    {
        $tree = array();
        $levelDifference = 1;

        foreach($categories as $category)
        {
            $categoryLeft = $category['left'];
            $categoryRight = $category['right'];
            $categoryLevel = $category['level'];

            if( $categoryLeft > $node['left']
                && $categoryRight < $node['right']
                && abs($node['level'] - $categoryLevel) === $levelDifference) {
                array_push($tree, array(
                    'id'        =>  $category['id'],
                    'title'     =>  $category['title'],
                    'url'       =>  $category['url'],
                    'children'  =>  $this->categoriesToTree($categories, $category)
                ));
            }

        }

        return $tree;
    }

    public function getPageByUrl($url)
    {
        $model = null;

        $criteria = new CDbCriteria();

        $criteria->condition = 'url=:url';
        $criteria->params = array(
            ':url'  =>  $url
        );

        $model = $this->find($criteria);

        if($model) {
            return $model->page;
        } else {
            return $model;
        }
    }
}