<?php

/**
 * This is the model class for table "page".
 *
 * The followings are the available columns in table 'page':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $page_title
 * @property string $page_description
 * @property string $page_keywords
 * @property integer $page_index
 *
 * The followings are the available model relations:
 * @property Category[] $categories
 */
class Page extends CActiveRecord implements SearchActiveRecord, SEOInterface
{
	use Search;
	use SEO;

	protected $templateFile = '_coffee_list';
	protected $renderObjectName = 'items';
	protected $controller = 'KatalogController';
	protected $uniqueSearchKey = 'brands';

    public $url;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Page the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function behaviors(){
		return array(
			'urlBehavior' => array(
				'class' => 'application.components.behaviors.UrlBehavior',
				'controller' => 'kofe'
			),
			'imageBehavior' => array(
				'class' => 'application.components.behaviors.ImageBehavior',
				'imagesFolder' => 'items'
			)
		);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'page';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, description, image', 'required'),
			array('title, image', 'length', 'max'=>100),
			array('page_title, page_keywords', 'length', 'max'=>255),
			array('page_index', 'numerical', 'integerOnly'=>true),
			array('page_description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, description, image, page_title, page_description, page_keywords, page_index', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categories' => array(self::HAS_MANY, 'Category', 'page_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'description' => 'Description',
			'image' => 'Image',
			'page_title' => 'Page Title',
			'page_description' => 'Page Description',
			'page_keywords' => 'Page Keywords',
			'page_index' => 'Page Index',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('page_title',$this->page_title,true);
		$criteria->compare('page_description',$this->page_description,true);
		$criteria->compare('page_keywords',$this->page_keywords,true);
		$criteria->compare('page_index',$this->page_index);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getAllBrandsProvider($parentId)
    {
        $brand = Category::model()->getNodeById($parentId);

        $criteria = new CDbCriteria();

        $criteria->together = true;
        $criteria->with = 'categories';
        $criteria->select = 'categories.url AS url, t.title, t.id, t.image';
        $criteria->addCondition('categories.page_id IS NOT NULL');
        $criteria->addCondition('categories.left > :left');
        $criteria->addCondition('categories.right < :right');

        $criteria->params = array(
            "left"  => $brand['left'],
            "right" => $brand['right']
        );

        return  new CActiveDataProvider('Page', array(
            'criteria'  =>   $criteria,
            'pagination'=>array(
                'pageSize'  =>  5,
            ),
        ));
    }

    public function isBrand()
    {
        return true;
    }
}