<?php

/**
 * This is the model class for table "article".
 *
 * The followings are the available columns in table 'article':
 * @property integer $id
 * @property string $title
 * @property string $date
 * @property string $body
 * @property string $description
 * @property string $image
 * @property string $url
 * @property string $page_title
 * @property string $page_description
 * @property string $page_keywords
 * @property integer $page_index
 */
class Article extends CActiveRecord implements SearchActiveRecord, SEOInterface
{
	use Search;
	use SEO;

    protected $templateFile = '_article_list';
	protected $renderObjectName = 'articles';
	protected $controller = 'StatiController';
	protected $uniqueSearchKey = 'articles';

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Article the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function behaviors(){
		return array(
			'urlBehavior' => array(
				'class' => 'application.components.behaviors.UrlBehavior',
				'controller' => 'stati'
			),
			'imageBehavior' => array(
				'class' => 'application.components.behaviors.ImageBehavior',
				'imagesFolder' => 'items'
			)
		);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'article';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, body, description, image', 'required'),
			array('title', 'length', 'max'=>100),
			array('description, image', 'length, page_title, page_keywords', 'max'=>255),
			array('url', 'length', 'max'=>45),
			array('page_description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, date, body, description, image, url, page_title, page_description, page_keywords, page_index', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'date' => 'Date',
			'body' => 'Body',
			'description' => 'Description',
			'image' => 'Image',
			'url' => 'Url',
			'page_title' => 'Page Title',
			'page_description' => 'Page Description',
			'page_keywords' => 'Page Keywords',
			'page_index' => 'Page Index',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('page_title',$this->page_title,true);
		$criteria->compare('page_description',$this->page_description,true);
		$criteria->compare('page_keywords',$this->page_keywords,true);
		$criteria->compare('page_index',$this->page_index);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getDate($prepared=false)
    {
        if($prepared) {
            $date = new DateTime($this->date);
            return $date->format('d M, Y');
        } else {
            return $this->date;
        }
    }

    public function getByUrl($url)
    {
        $article = null;

        $sql = 'SELECT *
                FROM `article`
                WHERE url=:url;';

        $article = Yii::app()->db
                            ->createCommand($sql)
                            ->bindParam(':url', $url)
                            ->queryRow();

        return $article;
    }
}