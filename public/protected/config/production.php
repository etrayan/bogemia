<?php

return CMap::mergeArray(
    require(dirname(__FILE__).'/main.php'),
    array(
        'components'=>array(
            'fixture'=>array(
                'class'=>'system.test.CDbFixtureManager',
            ),
            'db'=>array(
                'connectionString' => 'mysql:host=sql-4.radyx.ru;dbname=eduard618',
                'emulatePrepare' => true,
                'username' => 'eduard618',
                'password' => 'sfyz9m7rh4',
                'charset' => 'utf8',
	            'queryCacheID' => 'dbcache'
            ),
        ),
    )
);