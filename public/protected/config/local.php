<?php

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

return CMap::mergeArray(
    require(dirname(__FILE__).'/main.php'),
    array(
        'components'=>array(
            'fixture'=>array(
                'class'=>'system.test.CDbFixtureManager',
            ),
            'db'=>array(
                'connectionString' => 'mysql:host=localhost;dbname=bogemia',
                'emulatePrepare' => true,
                'username' => 'root',
                'password' => '1111',
                'charset' => 'utf8',
            ),
        ),
    )
);