<?php
$jui_theme = "smoothness";

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Интернет-магазин кофе',

	'preload'=>array('log'),

	'import'=>array(
		'application.models.*',
		'application.controllers.*',
		'application.components.*',
		'application.components.interfaces.*',
		'application.components.traits.*',
	),

    'theme'=>'coffee',

    'defaultController'=>'Site',
    'language'=>'ru',
    'sourceLanguage'=>'ru',
    'charset'=>'utf-8',

	'modules'=>array(

		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'xsw2cde3',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
	),

	// application components
	'components'=>array(
        'assetManager'=>array(
            'basePath'=>dirname(__FILE__).'/../../assets/',
            'baseUrl'=>'/assets/'
        ),

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
            'class' => 'WebUser',
		),
        'clientScript'=>array(
	        'class' => 'ext.min.EClientScript',
	        'combineScriptFiles' => true, // By default this is set to true, set this to true if you'd like to combine the script files
	        'combineCssFiles' => true, // By default this is set to true, set this to true if you'd like to combine the css files
	        'optimizeScriptFiles' => true, // @since: 1.1
	        'optimizeCssFiles' => true, // @since: 1.1
	        'optimizeInlineScript' => false, // @since: 1.6, This may case response slower
	        'optimizeInlineCss' => false, // @since: 1.6, This may case response slower
            'packages' => array(
                'jquery'=>array(
                    'baseUrl'=>'/js/jquery/jquery/',
                    'js'=>array('jquery.min.js'),
                ),
                'jqueryui'=>array(
                    'baseUrl'=>'/js/jquery/jqueryui',
                    'js'=>array('jquery-ui.min.js'),
                ),
	            'jquery.templates'=>array(
		            'baseUrl'=>'/js/jquery/template',
		            'js'=>array('jquery.tmpl.min.js'),
	            ),
                'site.js'=>array(
                    'baseUrl'=>'/js/',
                    'js'=>array('site.js'),
                ),
            ),
        ),
        'widgetFactory'=>array(
        ),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
            'showScriptName'=>false,
			'rules'=>array(
                'gii'=>'gii',
                'gii/<controller:\w+>'=>'gii/<controller>',
                'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
                '/'=>'site/index',
                'stati/<url>'=>'stati/index',
                'stati'=>'stati/index',
                'katalog/<url>'=>'katalog/index',
                'katalog'=>'katalog/index',
                'kofe/<url>'=>'kofe/index',
                'brendyi'=>'brendyi/index',
				'<action:\w+>'=>'site/<action>',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
            ),

	    ),
        'errorHandler'=>array(
            'errorAction'=>'site/error',
        ),
        'request'=>array(
            'enableCsrfValidation'=>true,
            //            'enableCookieValidation'=>true
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                ),
                // uncomment the following to show log messages on web pages
//                array(
//                    'class'=>'CWebLogRoute',
//                ),

            ),
        ),
    ),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'    =>  'etrayan@coffee.by',
        'images'    =>  array(
            'absolute'  =>  str_replace('protected' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . '', 'images', dirname(__FILE__)),
            'relative'  =>  '/images/'
        )
    ),


);