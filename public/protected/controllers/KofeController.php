<?php

class KofeController extends Controller implements Renderer
{
    public function actionIndex($url)
    {
        $isBrand = false;

        $item = Item::model()->getItemByUrl($url);

        if(!$item) {
            $isBrand = true;

            $item = Category::model()->getPageByUrl($url);

            if(!$item) {
                throw new CHttpException(404, 'Unable to find requested item');
            }
        }

	    $this->setMetaData('title', $item->getPageTitle());
	    $this->setMetaData('description', $item->getPageDescription());
	    $this->setMetaData('keywords', $item->getPageKeywords());

        $this->render('item', array(
            'item'  =>  $item,
            'isBrand'  =>  $isBrand,
        ));

    }
}