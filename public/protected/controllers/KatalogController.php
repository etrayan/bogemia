<?php


class KatalogController extends Controller implements Renderer
{
    public function getViewPath()
    {
        return 'themes/coffee/views/katalog';
    }

    public function actionIndex()
    {
        $items = array();
        $url = Yii::app()->request->getParam('url', false);

        if($url) {
            $items = Item::model()->getProviderByCategoryUrl($url);
        } else {
            $items = Item::model()->getAllProvider();
        }

	    $this->setMetaData('title', 'Bogemia - интернет-магазин кофе в Минске');
	    $this->setMetaData('description', 'Наш интернет-магазин доставит кофе в любую точку минска совершенно бесплатно');
	    $this->setMetaData('keywords', 'кофе, минск, магазин, интернет-магазин, доставка');

        $this->render('catalog', array(
            'items'     =>  $items,
            'url'       =>  $url,
            'title'     =>  'Каталог',
            'type'      =>  'widget'
        ));
    }
}