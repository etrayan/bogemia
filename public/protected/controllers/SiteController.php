<?php

class SiteController extends Controller
{

	public function actionIndex()
	{
        $hotItems = Item::model()->getHotItems();
        $saleItems = Item::model()->getSaleItems();
        $controller = new KatalogController(2);
        $controller->layout = false;

		$this->setMetaData('title', 'Bogemia - интернет-магазин кофе в Минске');
		$this->setMetaData('description', 'Наш интернет-магазин доставит кофе в любую точку минска совершенно бесплатно');
		$this->setMetaData('keywords', 'кофе, минск, магазин, интернет-магазин, доставка');

		$this->render('index', array(
            'hot'       =>  $hotItems,
            'sale'      =>  $saleItems,
            'renderer'  =>  $controller
        ));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{

		$this->render('contact');
	}

    public function actionCart()
    {
        $report = new OrderReport();
        $cart = new CartPreview();
        $cart->init();

        Yii::app()->params['cart'] = $cart;

        $model = new Order();

        $this->performAjaxValidation($model, 'checkout-form');

        if(isset($_POST['Order'])) {
            $model->attributes = $_POST['Order'];
            $model->price = $cart->getTotalPrice();

            if($model->save()) {
                $model->bindItemsFromCart($cart);

                $report->bindOrder($model);
                $report->bindCart($cart);
                $report->save();

                Yii::app()->user->setFlash('success', "Ваш заказ принят!");
                $this->redirect('report');
            } else {
                Yii::app()->user->setFlash('error', "Произошла ошибка при заполнении формы");
            }

        }

        if(count($cart->getItems()) > 0) {
            $this->render('full_cart', array(
                'cart'  =>  $cart,
                'model' =>  $model
            ));
        } else {
            $this->render('empty_cart');
        }
    }

    protected function performAjaxValidation($model, $key)
    {
        if(isset($_POST['ajax']) && $_POST['ajax'] === $key)
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionInsert()
    {
//        for($i=0;$i<20;$i++) {
//            $model = new Article();
//            $model->title = 'Статья номер ' . $i;
//            $model->image = '1.png';
//            $model->body = 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.';
//            $model->description = 'y of type and scrIt has survived n the 1960s with the release of Letraset sheets containing Lorem Ipsum passages';
//            $model->save();
//            CVarDumper::dump($model->getErrors());
//
//        }
        throw new CHttpException(404);
        /*
        // Создаем корень всего дерева. имя - root
        $root = new Category();
        $root->name = 'root';
        $root->saveNode();

        // Создаем 2 ветви от рута - производители и бренды
        $root = Category::model()->findByPk(1);

        $brands = new Category();
        $brands->name = 'Бренды';

        $makers = new Category();
        $makers->name = 'Производители';

        $brands->appendTo($root);
        $makers->appendTo($root);

        // Получаем все ветви от рута, и заполняем их данными рандомными
        $brands = Category::model()->findByPk(2);
        $makers = Category::model()->findByPk(3);

        for($i=0;$i<20;$i++) {
            $brand = new Category();
            $brand->name = "Бренд " . $i;
            $brand->appendTo($brands);
        }

        for($i=0;$i<5;$i++) {
            $maker = new Category();
            $maker->name = "Производитель " . $i;
            $maker->appendTo($makers);
        }

        // Заполняем товары....
        for($i=0;$i<230;$i++) {
            $item = new Item();
            $item->title = 'Товар ' . $i;
            $item->description = 'Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана. Маленький ручеек Даль журчит по всей стране и обеспечивает ее всеми необходимыми правилами. Эта парадигматическая страна, в которой жаренные члены предложения залетают прямо в рот. Даже всемогущая пунктуация не имеет власти над рыбными текстами, ведущими безорфографичный образ жизни. Однажды одна маленькая строчка рыбного текста по имени Lorem ipsum решила выйти в большой мир грамматики. Великий Оксмокс предупреждал ее о злых запятых, диких знаках вопроса и коварных точках с запятой, но текст не дал сбить себя с толку. Он собрал семь своих заглавных букв, подпоясал инициал за пояс и пустился в дорогу. Взобравшись на первую вершину курсивных гор, бросил он последний взгляд назад, на силуэт своего родного города Буквоград, на заголовок деревни Алфавит и на подзаголовок своего переулка Строчка. Грустный реторический вопрос скатился по его щеке и он продолжил свой путь. По дороге встретил текст рукопись. Она предупредила его: «В моей стране все переписывается по несколько раз. Единственное, что от меня осталось, это приставка «и». Возвращайся ты лучше в свою безопасную страну». Не послушавшись рукописи, наш текст продолжил свой путь. Вскоре ему повстречался коварный составитель';
            $item->price = $i * 1000;
            $item->image = '1.jpg';
            $item->save();
        }

        // Получаем все бренды и товары, и связываем их друг с другом
        $brands = Category::model()->findByPk(2)->children()->findAll();
        $items = Item::model()->findAll();

        for($i=0;$i<count($brands);$i++) {
            $brand = $brands[$i];

            if($i%2) {
                $count = 20;
            } else {
                $count = 3;
            }

            for($j=0;$j<$count;$j++) {
                $item = array_pop($items);
                $ci = new CategoryItem();
                $ci->item_id = $item->id;
                $ci->category_id = $brand->id;
                $ci->save();
            }
        }


        $brand = Category::model()->findByPk(4);

        for($k=0;$k<9;$k++) {
            $subBrand = new Category();
            $subBrand->name = 'Под Бренд 0-'. $k;
            $brand->append($subBrand);
        }
        */
    }

//    public function actionTest()
//    {
//        $model = Category::model();
//        $model->disableBehavior('NestedSetBehavior');
//
//        $nodes = $model->findAll();
//
//        foreach($nodes as $node) {
//            $node->save();
//        }
//    }

    public function actionReport()
    {
        $orderReport = new OrderReport();
        $report = $orderReport->extract();

        $cart = new CartPreview();

        if($report) {
            $cart->trash();
            $orderReport->trash();

            $this->render('report', array(
                'report'    =>  $report
            ));
        } else {
            $this->redirect('/');
        }
    }

    public function actionFeedback()
    {
        $model = new Feedback();

        $this->performAjaxValidation($model, 'feedback-form');

        if(isset($_POST['Feedback'])) {
            $model->attributes = $_POST['Feedback'];

            if($model->save()) {
                Yii::app()->user->setFlash('success', "Спасибо за ваше сообщение!");

                $this->redirect(Yii::app()->request->getUrl());
            } else {
                Yii::app()->user->setFlash('error', "Произошла ошибка при заполнении формы");
            }

        }

        $this->render('feedback', array(
            'model' =>  $model
        ));
    }

    public function actionSearch()
    {
        $query = Yii::app()->request->getParam('search-field', '');

        $searchEngine = new SearchEngine();
        $searchEngine->setQuery($query);
        $searchEngine->setSearchSources(array(
                Item::model(),
                Article::model(),
                Page::model()
        ));

        $result = $searchEngine->getResult();

        $this->render('search', array(
            'result'    =>  $result,
            'query'     =>  $query
        ));
    }
}