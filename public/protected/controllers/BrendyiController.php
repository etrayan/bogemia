<?php


class BrendyiController extends Controller implements Renderer
{
    public function getViewPath()
    {
        return 'themes/coffee/views/katalog';
    }

    public function actionIndex()
    {
        $brands = array();

        $brands = Page::model()->getAllBrandsProvider(1);

        $this->render('catalog', array(
            'items'     =>  $brands,
            'title'     =>  'Бренды',
            'type'      =>  'widget',
        ));
    }
}