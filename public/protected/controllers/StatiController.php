<?php


class StatiController extends Controller implements Renderer
{
    public function getViewPath()
    {
        return 'themes/coffee/views/stati';
    }

    public function actionIndex()
    {
        $url = Yii::app()->request->getParam('url', false);

        if($url) {
            $this->renderSingle($url);
        } else {
            $this->renderAll();
        }

        return;
    }

    private function renderAll()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'date DESC';

        $dataProvider = new CActiveDataProvider('Article', array(
            'criteria'  =>   $criteria,
            'pagination'=>array(
                'pageSize'  =>  5,
            ),
        ));

        $this->render('articles', array(
            'articles'  =>  $dataProvider,
            'type'      =>  'widget'
        ));
    }

    private function renderSingle($url)
    {
        $article = Article::model()->getByUrl($url);

        $this->render('article', array(
            'article'   =>  $article
        ));
    }
}