<?php

class AjaxController extends Controller
{

    public function actionGetItem()
    {
        $itemInfo = array();

        if(!Yii::app()->request->isAjaxRequest) {
            throw new CHttpException(404);
        }

        $id = Yii::app()->request->getParam('id', false);

        if($id) {
            $itemInfo = Item::model()->getItemInfoForAjax($id);
        } else {
            throw new CHttpException(404);
        }

        echo CJSON::encode($itemInfo);
    }

}