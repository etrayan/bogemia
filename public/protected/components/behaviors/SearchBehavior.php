<?php

class SearchBehavior extends CActiveRecordBehavior
{
	public $templateFile;
	public $renderObjectName;
	public $renderer;
	public $uniqueSearchKey;

	public function getRenderTemplateFile()
	{
		return $this->templateFile;
	}

	public function getRenderObjectName()
	{
		return $this->renderObjectName;
	}

	public function getRenderer()
	{
		$controller = new $this->controller(1);
		$controller->layout = false;

		return $controller;
	}

	public function getUniqueKey()
	{
		return $this->uniqueSearchKey;
	}
} 