<?php

class UrlBehavior extends CActiveRecordBehavior
{
	public $controller;

	protected function cyrillicToLatin($string)
	{
		$latin = '';

		$matrix = array(
			"Ґ"=>"G","Ё"=>"YO","Є"=>"E","Ї"=>"YI","І"=>"I",
			"і"=>"i","ґ"=>"g","ё"=>"yo","№"=>"#","є"=>"e",
			"ї"=>"yi","А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
			"Д"=>"D","Е"=>"E","Ж"=>"ZH","З"=>"Z","�?"=>"I",
			"Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
			"О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
			"У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
			"Ш"=>"SH","Щ"=>"SCH","Ъ"=>"'","Ы"=>"YI","Ь"=>"",
			"Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
			"в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"zh",
			"з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
			"м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
			"с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
			"ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"'",
			"ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya"
		);

		$latin = preg_replace('/[^a-z0-9]{1,}/', '-', strtolower(strtr($string, $matrix)));

		if ($latin[strlen($latin) - 1] == "-") {
			$latin = substr($latin, 0, strlen($latin) - 1);
		}

		return $latin;
	}

	public function beforeSave($event)
	{
		$this->owner->url = $this->cyrillicToLatin($this->owner->title);
	}

	public function getUrl($prepared=false)
	{
		if($prepared) {
			return Yii::app()->createUrl($this->getRoute(), array(
				'url'  =>  $this->owner->url
			));
		} else {
			return $this->owner->url;
		}
	}

	private function getRoute()
	{
		return $this->controller . '/index';
	}
} 