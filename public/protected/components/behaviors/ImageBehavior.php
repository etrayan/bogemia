<?php

class ImageBehavior extends CActiveRecordBehavior
{
	public $imagesFolder;

	protected function getImagesFolder()
	{
		return Yii::app()->params->images['relative'] . DIRECTORY_SEPARATOR . $this->imagesFolder . DIRECTORY_SEPARATOR;
	}

	public function getImage($prepared=false, $size='')
	{
		if($prepared) {
			return $this->getImagesFolder() . $this->owner->image;
		} else {
			return $this->owner->image;
		}
	}
}