<?php


trait Search
{
	public function getRenderTemplateFile()
	{
		return $this->templateFile;
	}

	public function getRenderObjectName()
	{
		return $this->renderObjectName;
	}

	public function getRenderer()
	{
		$controller = new $this->controller(1);
		$controller->layout = false;

		return $controller;
	}

	public function getUniqueKey()
	{
		return $this->uniqueSearchKey;
	}

	public function getResultsBySearchQuery($query)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'title LIKE :title';
		$criteria->params = array(
			':title'    =>  '%' . $query . '%'
		);

		return $this->findAll($criteria);
	}
} 