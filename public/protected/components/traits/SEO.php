<?php

trait SEO
{
	public function getPageTitle()
	{
		return $this->page_title;
	}

	public function getPageDescription()
	{
		return $this->page_description;
	}

	public function getPageKeywords()
	{
		return $this->page_keywords;
	}

	public function isPageIndex()
	{
		return $this->page_index === 1;
	}
} 