<?php

interface SearchActiveRecord
{
    public function getResultsBySearchQuery($query);

    public function getRenderTemplateFile();

    public function getRenderObjectName();

    public function getRenderer();

    public function getUniqueKey();
}