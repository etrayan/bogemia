<?php

interface SEOInterface
{
	public function getPageTitle();

	public function getPageDescription();

	public function getPageKeywords();

	public function isPageIndex();
} 