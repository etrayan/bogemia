<?php

interface SearchInterface
{
    public function setQuery($query);

    public function setSearchSources(array $sources);

    public function getResult();
}