<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	public $breadcrumbs = array();

	private $metaData = array();

	public function setMetaData($name, $value)
	{
		$this->metaData[$name] = $value;
	}

	public function beforeRender()
	{
		if(isset($this->metaData['title']))
		{
			$this->pageTitle = $this->metaData['title'];
			unset($this->metaData['title']);
		}

		foreach($this->metaData as $name => $value) {
			Yii::app()->clientScript->registerMetaTag($value, $name);
		}

		return true;
	}
}