<?php

class CurrencyHelper
{
    public static function formatBLR($price)
    {
        return number_format($price, 0, '.', ' ');
    }
}