<?php

class SearchEngine implements SearchInterface
{
    private $query;

    private $sources;

    private $result;

    private $renderType = 'list';

    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    public function setSearchSources(array $sources)
    {
        $this->sources = $sources;

        return $this;
    }

    public function getResult()
    {
        if(is_null($this->result)) {
            $this->result = $this->process();
        }

        return $this->result;
    }

    private function process()
    {
        $result = array();
        $query = $this->query;

        foreach($this->sources as $source) {
            if($source instanceof SearchActiveRecord) {

                $results = $source->getResultsBySearchQuery($query);
                $view = $source->getRenderTemplateFile();
                $renderer = $source->getRenderer();
                $object = $source->getRenderObjectName();
                $uniqueKey = $source->getUniqueKey();

                if($renderer instanceof Renderer && count($results) > 0) {
                    $result[$uniqueKey] = $renderer->renderPartial($view, array(
                        $object =>  $results,
                        'type'  =>  $this->renderType
                    ), true);
                }
            }
        }

        return $result;
    }
}