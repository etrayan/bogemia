<?php


class CartPreview
{
    private $items = array();

    private $totalPrice = 0.00;

    private $totalCount = 0;

    private $id;

    private $date;

    const ITEM_COOKIE_NAME = 'items';

    const ITEMS_COUNT = 4;

    public function setDate($date)
    {
        $this->date = $date;
    }

    public function getDate()
    {
        return $this->date->format('d M Y');
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function init()
    {
        $this->setItems();
    }

    public function getTotalPrice($prepared=false)
    {
        if($prepared) {
            return CurrencyHelper::formatBLR($this->totalPrice);
        } else {
            return $this->totalPrice;
        }
    }

    public function getTotalCount()
    {
        return $this->totalCount;
    }

    /**
     * @return array|Item|mixed|null
     */
    public function getItems()
    {
        return $this->items;
    }

    public function trash()
    {
        unset(Yii::app()->request->cookies[self::ITEM_COOKIE_NAME]);
    }

    public function getMaxItems()
    {
        return self::ITEMS_COUNT;
    }

    private function setItems()
    {
        $itemsInfo = $this->getItemsInfo();
        $cartItem = null;

        $this->totalCount = array_sum(array_values($itemsInfo));

        $items = Item::model()->getItemsByIds(array_keys($itemsInfo), true);

        foreach($items as $item) {
            $count = $itemsInfo[$item->id]|0;
            $item->setCount($count);

            $this->totalPrice += $item->getTotalPrice();

            array_push($this->items, $item);
        }
    }

    private function getItemsInfo()
    {
        $ids = array();
        $itemsInfo = array();

        $cookieString = Yii::app()->request->cookies[self::ITEM_COOKIE_NAME];

        $ids = CJSON::decode($cookieString);

        if(is_array($ids)) {
            $itemsInfo = array_count_values($ids);
        }

        return $itemsInfo;
    }
}