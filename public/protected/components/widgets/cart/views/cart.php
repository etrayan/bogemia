<div id="shopping-cart-block">
    <img src="<?php echo Yii::app()->params->images['relative'];?>/site/cart-icon.png" id="shopping-cart-icon" alt="Иконка корзины" />
    <div id="shopping-cart-preview">
        <a href="<?php echo Yii::app()->createUrl('site/korzina');?>" id="shopping-cart-link-expand" class="menu-element">
            <span id="cart-preview-items-count"><?php echo $cart->getTotalCount(); ?></span> -
            <span id="cart-preview-total-price"><?php echo $cart->getTotalPrice(true); ?></span> бел.руб. &nbsp;&nbsp;
                    <span id="shopping-cart-button-expand">
                        <img src="<?php echo Yii::app()->params->images['relative'];?>/site/shop-cart-button.png" alt="Корзина" />
                    </span>
        </a>
    </div>
    <table id="cart-items-list">
	    <tfoot>
	    <tr id="show-more" class="cart-preview-title <?php if(count($cart->getItems()) <= 4) : ?>dn<?php endif; ?>">
		    <td colspan="3">
			    <a href="<?php echo Yii::app()->createUrl('site/cart'); ?>" class="cart-title-preview">
				    Просмотреть все
				    <span id="show-more-count"><?php echo count($cart->getItems()); ?></span>
				    товаров
			    </a>
		    </td>
	    </tr>
	    <tr>
		    <td colspan="3">ИТОГО: <span id="cart-preview-total"><?php echo $cart->getTotalPrice(true); ?></span> бел. руб.</td>
	    </tr>
	    <tr>
		    <td colspan="3">
			    <a href="<?php echo Yii::app()->createUrl('site/cart'); ?>">Перейти к оформлению заказа</a>
		    </td>
	    </tr>
	    </tfoot>
        <tbody>
            <tr class="dn"><td class="dn"></td></tr>
        <?php foreach($cart->getItems() as $key => $item) : ?>
            <tr class="cart-item-<?php echo $item->id; ?> <?php if($key >= $cart->getMaxItems()) : ?> dn <?php endif; ?>">
                <td class="cart-preview-title">
                    <a class="cart-title-preview" href="<?php echo $item->getUrl(true); ?>"><?php echo $item->getTitle(); ?></a>
                </td>
                <td class="cart-preview-count">
                    <a href="javascript: void(0)" onclick="cart.remove(<?php echo $item->getId(); ?>, 1)" class="minus-item">&#8211;</a>
                    <input type="text" class="cart-preview-number" value="<?php echo $item->getCount(); ?>" />
                    <input type="hidden" class="item-price" value="<?php echo $item->getPrice(); ?>" />
                    <a href="javascript: void(0)" onclick="cart.add(<?php echo $item->getId(); ?>, 1)" class="plus-item">+</a>
                </td>
                <td class="cart-preview-delete">
                    <a href="javascript: void(0)" onclick="cart.remove(<?php echo $item->getId(); ?>, 1000);">
                        <img src="<?php echo Yii::app()->params->images['relative'];?>/site/delete.png" alt="Х" />
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <input type="hidden" id="hidden-total-price" value="<?php echo $cart->getTotalPrice(); ?>" />
</div>

<div id="add-to-cart-notify">
    <p>Товар успешно добавлен в корзину</p>
</div>