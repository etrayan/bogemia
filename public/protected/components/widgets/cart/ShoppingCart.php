<?php


class ShoppingCart extends CWidget
{
    public function init()
    {

    }

    public function run()
    {
        if(isset(Yii::app()->params['cart']) && is_object(Yii::app()->params['cart']) && Yii::app()->params['cart'] instanceof ShoppingCart) {
            $cart = Yii::app()->params['cart'];
        } else {
            $cart = new CartPreview();
            $cart->init();
        }

        $this->render('cart', array(
            'cart'  =>  $cart
        ));
    }
}