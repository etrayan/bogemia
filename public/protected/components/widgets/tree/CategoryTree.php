<?php

class CategoryTree extends CWidget
{
    public function init()
    {

    }

    public function run()
    {
        $category = new Category();
        $tree = $category->getCategoriesAsTree();

        $treeView = $this->renderTree($tree, '');

        $this->render('categories', array(
            'treeView'  =>  $treeView
        ));
    }

    private function renderTree($tree, $view)
    {
        foreach($tree as $node) {
            $count = count($node['children']);

            if($count === 0) {
                $view .= $this->render('_li_final', array(
                    'node'  =>  $node
                ), true);

            } elseif($count > 0) {
                $view .= $this->render('_li_branch_start', array(
                    'node'  =>  $node
                ), true);

                $view = $this->renderTree($node['children'], $view);

                $view .= $this->render('_li_branch_end', array(), true);
            }
        }

        return $view;
    }
}