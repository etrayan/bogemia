<?php


class OrderReport
{
    private $items;

    private $totalPrice;

    private $subTotalPrice;

    private $order;

    private $orderBlackList = array(
        'status',
        'price'
    );

    private $translations = array(
        'id'        =>  'Номер вашего заказа',
        'courier'   =>  'Курьером',
        'mail'      =>  'Почтой',
        'pickup'    =>  'Самовывоз'
    );

    const REPORT_SESSION_NAME = 'report';

    public function getOrder()
    {
        return $this->order;
    }

    public function getItems()
    {
        return $this->items;
    }

    public function getSubTotalPrice()
    {
        return $this->subTotalPrice;
    }

    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    public function bindOrder(Order &$order)
    {
        $this->order = array();
        $labels = $order->attributeLabels();

        foreach($order as $name=>$value) {
            if(!is_null($value) && $value && !in_array($name, $this->orderBlackList)) {
                $this->order[$this->translate($labels[$name])] = $this->translate($value);
            }
        }
    }

    public function bindCart(CartPreview &$cart)
    {
        $this->items = array();

        $this->totalPrice = $cart->getTotalPrice(true);
        $this->subTotalPrice = $cart->getTotalPrice(true);

        $items = $cart->getItems();

        foreach($items as &$item) {

            array_push($this->items, array(
                'title' =>  $item->getTitle(),
                'count' =>  $item->getCount(),
                'price' =>  $item->getTotalPrice(true),
            ));

        }
    }

    public function save()
    {
        $session=new CHttpSession();
        $session->open();

        $session[self::REPORT_SESSION_NAME] = $this;
    }

    public function extract()
    {
        $session=new CHttpSession();
        $session->open();

        return $session[self::REPORT_SESSION_NAME];
    }

    public function trash()
    {
        $session=new CHttpSession();
        $session->open();

        $session->remove(self::REPORT_SESSION_NAME);
    }

    private function translate($string)
    {
        return isset($this->translations[$string]) ? $this->translations[$string] : $string;
    }
}